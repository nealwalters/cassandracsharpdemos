﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;
using System.Configuration;
using System.IO;

namespace CassandraFileTool
{

    /// <summary>
    ///     Author: Neal Walters (http://MyLifeIsMyMessage.net) Irving (Dallas) Texas - April 13, 2017 
    ///     Created in Visual Studio 2013 - 
    ///     
    ///     Purpose test two functions: 
    ///       1) ARCCHIVE - reading files from disk and storing as a blob in Cassandra databsae,
    ///       2) EXTRACT - reading rows/blobs from Cassandra and storing back to a specified disk directory 
    ///
    ///    Was just a Proof of Concept/Demo/Experiment, but tested and working.  
    ///    Doesn't handle nested directories. 
    ///    
    ///    Limitations: Apparently Cassandra community edition has limit of 64MB per blob, thus won't be able to 
    ///                 to archive/restore files over that size.  
    ///    
    ///    See the \CQLSCripts directory for CQL scripts that can be run in DevCenter to create the table/columns. 
    ///    Install and start Cassandra, then 
    ///    run script CreateFileArchive.cql before running this C# program. 
    ///
    ///  Command Line Parameters (or Project Properties/Debug/Start Options) for testing: 
    ///    Archive c:\apache-cassandra-3.10\TestArchiveDir2
    ///    Extract c:\apache-cassandra-3.10\TestArchiveDir  c:\apache-cassandra-3.10\TestArchiveDir\Restore\
    ///    Extract c:\apache-cassandra-3.10\TestArchiveDir2  c:\apache-cassandra-3.10\TestArchiveDir2\Restore\
    ///    
    ///  Data for testing is not included in the GitHub.  Use your own test directory with a mix of text and/or 
    ///  Office documents and/or zip files. 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            // This was just for testing the Read/Write without using Cassandra 
            //testFileReadWrite();
            //return;

            string dbAddress = null;
            string dbKeyspace = null;
            ISession session = null;
            try
            {
                // Get Cassandra database address and Keyspace from App.config file 
                dbAddress = ConfigurationManager.AppSettings["dbAddress"].ToString();
                dbKeyspace = ConfigurationManager.AppSettings["dbKeyspace"].ToString();
                Console.WriteLine("CasandraFileTool: dbAddress={0} dbKeyspace={1}", dbAddress, dbKeyspace);

                // Connect to the demo keyspace on our cluster running at 127.0.0.1
                Cluster cluster = Cluster.Builder().AddContactPoint(dbAddress).Build();
                Console.WriteLine("Connected to dbAddress");
                // looks like keyspacename (parm below) has to be all lower case 
                session = cluster.Connect(dbKeyspace);
                Console.WriteLine("Connected to dbKeyspace");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // Include next two lines when running in Visual Studio 
                //Console.WriteLine("\nCassandraFileTool: End");
                //Console.ReadLine();
                return;
            }


            if (args.Length < 2)
            {
                Console.WriteLine("Error: Invalid number command line arguments, expecting two; the first is command (Archive, Extract), the second is directory name.");
            }
            string strCommand = args[0];
            string strDirectory = args[1];


            if (strCommand.ToUpper() == "ARCHIVE")
            {
                if (Directory.Exists(strDirectory))
                {
                    ArchiveFiles(session, strDirectory);
                }
                else
                {
                    Console.WriteLine("Error: Archive directory does not exist: " + strDirectory);
                }
            }
            else
                if (strCommand.ToUpper() == "EXTRACT")
                {
                    if (args.Length < 3)
                    {
                        Console.WriteLine("Error: Invalid number command line arguments for EXTRACT, after the command 'EXTRACT', expecting from-dir (in DB table) and to-dir (on disk)");
                    }
                    string strToDirectory = args[2];
                    ExtractFiles(session, strDirectory, strToDirectory);
                }
                else
                {
                    Console.WriteLine("Invalid Command: " + strCommand + "; expecting ARCHIVE or EXTrACT");
                }

            // Include next two lines when running in Visual Studio 
            //Console.WriteLine("\nCassandraFileTool: End");
            //Console.ReadLine();

        }

        static void ArchiveFiles(ISession argSession, string argDirName)
        {
            //string[] files = Directory.GetFiles(argDirName, "*.*", SearchOption.TopDirectoryOnly);
            System.IO.DirectoryInfo dirInfo = new DirectoryInfo(argDirName);
            System.IO.FileInfo[] files = dirInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly);
            Console.WriteLine("Number of files=" + files.Length);
            foreach (FileInfo file in files)
            {
                Console.WriteLine("Filename=" + file.Name);

                byte[] fileContentsByteArray = File.ReadAllBytes(file.FullName); 
                string fileContents = File.ReadAllText(file.FullName, Encoding.Default);
                int fileContentsLen = fileContents.Length;
                string fileContentsCleaned = fileContents.Replace("'", "''");
                string fileExtensionFixed = file.Extension.Replace(".", "");

                /*
                string strCQL = String.Format("INSERT INTO filearchive (machine_name, file_folder, file_name, file_ext, row_stored_date, file_date_created, file_date_modified, file_contents) " +
                    " VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}',  textAsBlob('{7}')); ",
                    Environment.MachineName,
                    file.DirectoryName,
                    file.Name,
                    fileExtensionFixed,
                    System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                    file.CreationTimeUtc.ToString("yyyy-MM-dd HH:mm:ss"),
                    file.LastWriteTimeUtc.ToString("yyyy-MM-dd HH:mm:ss"),
                    fileContentsCleaned
                    );
                int posTextAsBlob = strCQL.IndexOf("textAsBlob");
                string strShowCQL = strCQL.Substring(0, posTextAsBlob + 11);
                */


                //Console.WriteLine(strCQL);
                try
                {
                    // example: var statement = session.Prepare("SELECT * FROM table where a = ? and b = ?");
                    var statement = argSession.Prepare(
                        "INSERT INTO filearchive " +
                        " (machine_name, file_folder, file_name, file_ext, row_stored_date, file_date_created, file_date_modified, file_contents) " +
                        " VALUES (?,?,?,?,?,?,?,?);"
                        );
                    // Bind parameter by marker position 
                    argSession.Execute(statement.Bind(
                        Environment.MachineName,
                        file.DirectoryName,
                        file.Name,
                        fileExtensionFixed,
                        System.DateTime.UtcNow,
                        file.CreationTimeUtc,
                        file.LastWriteTimeUtc,
                        fileContentsByteArray
                        ));
                }
                catch (Cassandra.InvalidConfigurationInQueryException ceex)
                {
                    Console.WriteLine(ceex.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }


        static void ExtractFiles(ISession argSession, string argFromDirName, string argToDirName)
        {
            //string[] files = Directory.GetFiles(argDirName, "*.*", SearchOption.TopDirectoryOnly);
            // Try data retrieval 
            string strCQL = String.Format("select * from filearchive where file_folder = '{0}' allow filtering", argFromDirName);
            RowSet rows = argSession.Execute(strCQL);
            foreach (Row row in rows)
            {
                string strArchFilename = row["file_name"].ToString();
                string strNewFilename = Path.Combine(argToDirName, strArchFilename).ToString();
                byte[] fileContentsByteArray = (byte[])row["file_contents"];
                Console.WriteLine("file={0} size={1}", strNewFilename, fileContentsByteArray.Length);
                File.WriteAllBytes(strNewFilename, fileContentsByteArray);

            }
        }

        static void testFileReadWrite()
        {
            // this was just to prove that we could read/write an Excel file back to disk in exact same format/bytes 

            string filenameIn = @"c:\apache-cassandra-3.10\TestArchiveDir2\ATG Timesheet 2017_04_03_NealWalters.xls";
            string filenameOut1 = @"c:\apache-cassandra-3.10\TestArchiveDir2\ATG Timesheet 2017_04_03_NealWalters_Test.xls";
            string filenameOut2 = @"c:\apache-cassandra-3.10\TestArchiveDir2\ATG Timesheet 2017_04_03_NealWalters_Test2.xls";
            string filenameOut3 = @"c:\apache-cassandra-3.10\TestArchiveDir2\ATG Timesheet 2017_04_03_NealWalters_Test3.xls";

            /*
            string fileContents1 = File.ReadAllText(filenameIn);
            File.WriteAllText(filenameOut1, fileContents1);

            string fileContents2 = File.ReadAllText(filenameIn, Encoding.Unicode);
            File.WriteAllText(filenameOut2, fileContents2, Encoding.Unicode);
             */

            byte[] fileContents3 = File.ReadAllBytes(filenameIn);
            File.WriteAllBytes(filenameOut3, fileContents3);


        }


    }
}
